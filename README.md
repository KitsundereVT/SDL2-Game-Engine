# SDL2-Game-Engine
Work in Progress!

Prerequisites for Building/Running:
SDL2 - https://wiki.libsdl.org/SDL2/FrontPage

SDL2_image - https://wiki.libsdl.org/SDL2_image/FrontPage 

SDL2_mixer - https://wiki.libsdl.org/SDL2_mixer/FrontPage 

-(Needed For The Compiler: include & lib folders)
-(Needed For The EXE: SDL2.dll, SDL2_image.dll, SDL2_mixer.dll)

Getting Started in Visual Studio:

Download SDL2, image, and mixer from above (get "devel-VC" zips from the GitHub repo's Releases) and either... 

A) Copy the folders named "include" & "lib" to "src" of the project folder.

OR

B) Extract the zips to a directory like "C:\SDL2" and setup your include/library directories as well as Linker settings in Project > Settings > C++ Directories (Include/Library Directories, add SDL2 paths) & Project > Settings > Linker > Input (Additional Dependencies, add "SDL2.lib;SDL2main.lib;SDL2_image.lib;SDL2_mixer.lib;" [without quotes])

~BE SURE YOU DROP THE REQUIRE SDL2 DLLS IN THE PATH WHERE THE PROGRAM'S .EXE FILE IS LOCATED BEFORE RUNNING~


29/Jan/2024 - Recently Separated the "test game" from the actual engine as to not confuse the two.
You can find most of the test game source in "./src/test_game" as well as obviously the main.cpp.


To my fellow dev team--
	It's very early, things are apt to change, and I hope I'm not too annoying about it.
	Thank you for taking interest and joining me on this journey!
	
	Some basic things we can do right now areeee:
	
	1) Create Sprites which load images of any kind into a 32-bit surface
	AND SpriteGroups to sort and control multiple Sprites simultaneously with ease
	
	--To create a new Sprite, simply do this
	Sprite spr_name("filename.png", x, y, sizing_multiplier[optional], speed[optional], type[optional]);
	//Currently the only Sprite Types are "Default" and "bg". "bg" is (soon) going to always be centered and stretch to the window dimensions with respect to aspect ratio.
	
	-You could also do something like initialize a pointer and set it later like this:
	Sprite* spr_name;
	spr_name = new Sprite("filename.png", x, y, sizing_multiplier[optional], speed[optional], type[optional]);
	
	--To create a new SpriteGroup:
	SpriteGroup spr_group = new SpriteGroup;
	
	-Again you can initialize a pointer and set it later:
	SpriteGroup* spr_group;
	spr_group = new SpriteGroup;
	
	--Adding/Removing Sprites to/from a SpriteGroup OR Clearing a SpriteGroup:
	//Assuming spr_name is a Sprite* and spr_group is a SpriteGroup
	spr_group.add(spr_name);
	spr_group.remove(spr_name);
	spr_group.empty();
	
	
	2) Move, resize, and do basic updates on sprites, as well as a sample DVD bounce "animation".
	
	--To MOVE a Sprite you can use:
	//This is a very basic x,y coordinate for image positioning
	spr_name.move(x,y);
	
	--To RESIZE a Sprite:
	//Not fully implemented YET
	
	--To UPDATE a Sprite or All Sprites (Good for Screen Reset Purposes like Window Resizing):
	spr_name.update(); // Update an individual Sprite
	OR
	spr_group.update(); // Updates all the Sprites
	
	3) Clean-up / Deconstruction (Boring technical junk)
	This just frees the SDL_Surface for a Sprite, for clean-up purposes:
	~Sprite();
	//Adding more soon for ease of use memory management
	
	
	I honestly forget if there's more, probably not much more yet. We'll work on getting more features shoved in here and fully fleshed out soon. Just wanted to make sure we have SOMETHING up online for us to work with~
	
	-Mz