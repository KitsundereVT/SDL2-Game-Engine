#include "Mz/Carnage.h"
#include "Mz/DispInit.h"

Uint32 white = NULL;

int InitDispEngine() {
    // Initialize SDL Video and Events (Update for more later...)
    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS) < 0) {
        cout << "Error Initializing SDL2: " << SDL_GetError() << endl;
        return 1;
    }
    // Init SDL_Image PNG (Update later for more filetypes as needed)
    if (IMG_Init(IMG_INIT_PNG) == 0) {
        cout << "SDL2_image Initialization Error: " << SDL_GetError() << endl;
        return 2;
    }
    
    // Window Creation
    window = SDL_CreateWindow("SDL2 Game Engine - Alpha 0.01", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, window_width, window_height, SDL_WINDOW_RESIZABLE);
    if (window == NULL) {
        cout << "Error Creating SDL Window: " << SDL_GetError() << endl;
        return 3;
    }
    else {
        SDL_GetWindowDisplayMode(window, wDisp);
        SDL_GetWindowSize(window, &oW, &oH);
        wW = oW;
        wH = oH;
    }

    // Renderer Creation
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    if (renderer == NULL) {
        cout << "Error Creating SDL Renderer: " << SDL_GetError() << endl;
        return 4;
    }

    // Screen Surface, Colors
    //screen = SDL_GetWindowSurface(window);
    //white = SDL_MapRGB(screen->format, 255, 255, 255);
    //SDL_FillRect(screen, NULL, white);
    //SDL_UpdateWindowSurface(window);

    //white = SDL_MapRGB((SDL_PixelFormat*)SDL_PIXELFORMAT_RGB888, 255, 255, 255);
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderClear(renderer);
    SDL_RenderPresent(renderer);

    return 0;
}