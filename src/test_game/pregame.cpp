#include "Mz/Carnage.h"
#include "Mz/Sprites.h"
#include "../test_game/pregame.h"

Sprite* spr3;
Sprite* spr;
Sprite* spr2;
SpriteGroup* active;


bool title = true;

void PreGame()
{
    active = new SpriteGroup;
// Load Sprites
    spr2 = new Sprite("imgs/title/rating.bmp", 0, 0, 1, 0, "bg");
    spr3 = new Sprite("imgs/title/logo2.bmp", 0, 0, 1, 0, "bg");
    spr = new Sprite("imgs/title/postal_8bit.bmp", 0, 0, 1, 0, "bg");
// Add Sprites to SpriteGroup
// AUTOMATE THIS IN THE FUTURE
    active->add(spr3);
    active->add(spr2);
    active->add(spr);
}

void Title() {
    active->slideshow(5000);
    spr2->~Sprite();
    spr3->~Sprite();
    
}

void DoGraphics() {
    if (title == true) {
        Title();
        title = false;
    }
    if (active->getSprites().size() >= 0) {
        active->render(screen_tex);
        presentRender();
    }
}



void ResetSprites()
{
    // Grab old dimensions before reset--
    active->update();
}

void SpriteCleanup()
{
    active->KillSprites();
}