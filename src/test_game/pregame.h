#ifndef PGAME
#define PGAME


void PreGame(void);
void DoGraphics(void);
void ResetSprites(void);
void SpriteCleanup(void);
void Title(void);

#endif //PGAME