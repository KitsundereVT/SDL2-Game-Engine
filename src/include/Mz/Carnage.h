#ifndef CARNAGE_H
#define CARNAGE_H

#include <iostream>
#include <algorithm>
#include <vector>
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_mixer.h>


extern bool toggle;

#define window_width 800
#define window_height 600

#define fps 60

extern SDL_Window* window;
extern SDL_Renderer* renderer;
extern SDL_Texture* screen_tex;
//extern SDL_Surface* screen;
extern SDL_DisplayMode* wDisp;
extern int wW, wH, oW, oH;
extern float aspectRatio;
extern SDL_TimerID timerID;
extern bool quit;
extern Uint32 timerCallback(Uint32, void*);
extern SDL_Event e;

void presentRender(void);
void PollEvents(SDL_Event);
void Toggle(void);

using namespace std;
#endif //CARNAGE_H