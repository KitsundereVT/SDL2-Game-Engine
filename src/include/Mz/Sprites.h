#ifndef SPRITES
#define SPRITES

class Sprite {

private:
    SDL_Texture* image;
    //SDL_Surface* image;
    SDL_Rect rect;

    int orig_w, orig_h,
        origin_x, origin_y,
        w, h,
        x, y,
        hSpeed, vSpeed, speed,
        newWidth, newHeight,
        newX, newY;
    double scaleX, scaleY, scale, sm;

    bool vert, horz;

    string type;
public:


    Sprite(const char* filename, int x, int y, double sm = 1.0, int speed = 0, string type = "default") :
        image(nullptr),
        orig_w(0), orig_h(0),
        origin_x(0), origin_y(0),
        w(0), h(0),
        x(x), y(y),
        hSpeed(speed), vSpeed(speed), speed(speed),
        newWidth(0), newHeight(0),
        newX(0), newY(0),
        scaleX(0.0), scaleY(0.0), scale(0.0),
        sm(sm),
        vert(false), horz(false), type("Default") {


        //Sprite::sm = sm;
        Sprite::type = type;
        // Load the image
        // image = IMG_Load(filename);
        image = IMG_LoadTexture(renderer, filename);
        SDL_QueryTexture(image, NULL, NULL, &orig_w, &orig_h);

        w = orig_w;
        h = orig_h;

        
        rect = { x,y,orig_w,orig_h };
    
        // Calculate the scaling factors for width and height using doubles
        scaleX = static_cast<double>(wW) / static_cast<double>(orig_w);
        scaleY = static_cast<double>(wH) / static_cast<double>(orig_h);

        // Choose the smaller of the two scaling factors to maintain aspect ratio
        scale = min(scaleX, scaleY);

        // Calculate the scaled dimensions
        w = static_cast<int>(orig_w*sm * scale);
        h = static_cast<int>(orig_h*sm * scale);

        if (type == "bg") {
            rect = { x,y,w,h };
        }
        else {
            
            // Setup the rect
            rect = { static_cast<int>(x*scale),static_cast<int>(y*scale),w,h };
            
        }
        
    }

    void update() {
  
        // Calculate the scaling factors for width and height using doubles
        scaleX = static_cast<double>(wW) / static_cast<double>(orig_w);
        scaleY = static_cast<double>(wH) / static_cast<double>(orig_h);

        // Choose the smaller of the two scaling factors to maintain aspect ratio
        scale = min(scaleX, scaleY);

        // Calculate the scaled dimensions
        w = static_cast<int>(orig_w*sm * scale);
        h = static_cast<int>(orig_h*sm * scale);

        if (type == "bg") {
            rect.w = w;
            rect.h = h;
            x = rect.x = (wW-w)/2;
            y = rect.y = (wH-h)/2;
        }
        else {
            rect.w = w;
            rect.h = h;
            rect.x = static_cast<int>(rect.x *scale);
            rect.y = static_cast<int>(rect.y *scale);
        }
        
    }

    void move(int x, int y) {
        rect.x = x;
        rect.y = y;
    }

    void moveImg() {
        if (speed > 0) {
            // DVD Bounce
            (x + w >= wW) ? horz = false : (x <= 0 ? horz = true : NULL);
            horz ? x += hSpeed : x -= hSpeed;
            (y + h >= wH) ? vert = false : ((y <= 0) ? vert = true : NULL);
            vert ? y += vSpeed : y -= vSpeed;

            move(x, y);
        }
    }

    void render() {
        if (image != nullptr) {
           SDL_RenderCopy(renderer, image, NULL, &rect);
        }
    }

    SDL_Surface* getImg() const {
        //return image;

    }

    SDL_Texture* getTexture() const {
        return image;
    }

    bool operator==(const Sprite& other) const {
        return (image == other.getTexture());
    }

    ~Sprite() {
        if (image != nullptr) {
            SDL_DestroyTexture(image);
            image = nullptr;
        }
    }
};

class SpriteGroup {
private:
    vector <Sprite*> sprites;
    size_t sprites_size = 0;

public:

    SpriteGroup copy() {
        SpriteGroup new_group;

        for (int i = 0; i < sprites_size; i++) {
            new_group.add(sprites[i]);
        }
        return new_group;
    }

    void add(Sprite* sprite) {
        sprites.push_back(sprite);
        sprites_size = sprites.size();
    }

    void remove(Sprite sprite_obj) {
        for (int i = 0; i < sprites_size; i++) {
            if (*sprites[i] == sprite_obj) {
                sprites.erase(sprites.begin() + i);
            }
        }
        sprites_size = sprites.size();
    }

    bool has(Sprite sprite_obj) {
        for (int i = 0; i < sprites_size; i++) {
            if (*sprites[i] == sprite_obj) {
                return true;
            }
        }
        return false;
    }

    void move(Sprite sprite_obj, int x, int y) {
        sprite_obj.move(x, y);
    }

    void moveImg(int x, int y) {
        for (int i = 0; i < sprites_size; i++) {
            sprites[i]->moveImg();
        }

    }

    void update() {
        if (!sprites.empty()) {
            for (int i = 0; i < sprites_size; i++) {
                sprites[i]->update();
            }
        }
    }

    void render(SDL_Texture* dst) {
        SDL_SetRenderTarget(renderer, dst);
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
        SDL_RenderFillRect(renderer, NULL);

        if (!sprites.empty()) {
            for (int i = 0; i < sprites_size; i++) {
                sprites[i]->render();
            }
        }
        SDL_SetRenderTarget(renderer, NULL);
    }

    void empty() {
        sprites.clear();
        sprites_size = sprites.size();
    }

    vector <Sprite*> getSprites() {
        return sprites;
    }

    void slideshow(int duration) {
        if (!sprites.empty()) {
            for (int i = 0; i < sprites_size; i++) {
                SDL_SetRenderTarget(renderer, screen_tex);
                SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
                SDL_RenderFillRect(renderer, NULL);
                sprites[i]->render();
                SDL_SetRenderTarget(renderer, NULL);
                presentRender();
                timerID = SDL_AddTimer(duration, timerCallback, NULL);
                toggle = true;
                Toggle();
                SDL_RemoveTimer(timerID);
            }
        }

    }

    void KillSprites() {
        for (int i = 0; i < sprites_size; i++) {
            sprites[i]->~Sprite();
        }
    }

};

#endif //SPRITES