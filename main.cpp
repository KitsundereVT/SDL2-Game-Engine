// main.cpp - Carnage Engine Test Project
//
// 
// Engine Library
#include "Mz/Carnage.h"
#include "Mz/DispInit.h"
#include "Mz/Sprites.h"
#include "Mz/GameObject.h"
// Test Program
#include "../test_game/pregame.h"

SDL_Window* window = nullptr;
SDL_Renderer* renderer = nullptr;
SDL_Texture* screen_tex = nullptr;
SDL_DisplayMode *wDisp = new SDL_DisplayMode;
int wW, wH, oW, oH = 0;
float aspectRatio = 0.0;
SDL_Event e;
SDL_TimerID timerID;
bool quit = false,
toggle = false;

// Clean up SDL. Destroy Windows, Renderers, Surfaces, Textures, Etc. Prior To Exit...
void cleanUpSDL() {
    SpriteCleanup();
    SDL_DestroyTexture(screen_tex);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    IMG_Quit();
    SDL_Quit();
}


Uint32 timerCallback(Uint32 interval, void* param) {
    toggle = false;

    return 0;
}

void Toggle() {
    while (toggle) {
        if (SDL_PollEvent(&e)) {
            if (e.type == SDL_QUIT) {
                quit = true;
                break;
            }
            PollEvents(e);
        }
    }
}
void PollEvents(SDL_Event e) {
        if (e.type == SDL_WINDOWEVENT) {
            if (e.window.event == SDL_WINDOWEVENT_RESIZED || e.window.event == SDL_WINDOWEVENT_MAXIMIZED) {
                SDL_DestroyTexture(screen_tex);
                SDL_GetWindowSize(window, &wW, &wH);
                screen_tex = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, wW, wH);
                ResetSprites();
            }
        }
}

// Currently Unused...
void presentRender() {
    //SDL_RenderClear(renderer);
    SDL_RenderCopy(renderer, screen_tex, NULL, NULL);
    SDL_RenderPresent(renderer);
}

void limitFPS(int f, int tick) {
    if (f == NULL)
        f = fps;

    if (static_cast<Uint32>(1000 / f) > SDL_GetTicks() - tick) {
        SDL_Delay(1000 / f - (SDL_GetTicks() - tick));
    }
}

// Our main entry function for the app. Everything starts HERE!
int main(int argc, char* argv[])
{
    // Initialize all the things!
    int sResult;
    sResult = InitDispEngine();

    screen_tex = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, wW, wH);

    PreGame();
    ResetSprites();
    // MAIN EVENT LOOP
    Uint32 starting_tick;
    

    while (!quit) {
        starting_tick = SDL_GetTicks();

        if (SDL_PollEvent(&e)) {
            if (e.type == SDL_QUIT) {
                quit = true;
                break;
            }
            PollEvents(e);

            SDL_RenderClear(renderer);

            // Process Graphics Hereeee--
            // TEMPORARY FUNCTION FOR GRAPHICS PROCESSING IN PreGame.cpp
            DoGraphics();
            presentRender();

            // Limit FPS
            limitFPS(60, starting_tick);

        }
        // delete sprites, clean up SDL
    }
        cleanUpSDL();
        // End of Program~
        return 0;
}
